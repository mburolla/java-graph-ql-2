# Java GraphQL

# Links
[YouTube](https://youtu.be/zX2I7-aIldE)

# Notes
POST http://localhost:8080/rest/books

Request
```
{
  allBooks {
    isn
    title
  }
}
```
Response
```   
{
  "errors": [],
  "data": {
    "allBooks": [
      {
        "isn": "123",
        "title": "Book of clouds1"
      },
      {
        "isn": "345",
        "title": "Book of clouds2"
      },
      {
        "isn": "456",
        "title": "Book of clouds3"
      }
    ]
  },
  "extensions": null
}
```

```
query {
  book(id: "123") {
    title
  }
}
```

```
query {
  allBooks  {
    title
  }
  book(id: "123") {
    title
    authors
  }
}
```